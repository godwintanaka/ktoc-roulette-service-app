package com.godwin.playsafe.tech.tests.service.api;

/**
 * @author Godwin Tavirimirwa
 * created at 19-Dec-20 07:14
 */
public interface PartOneService {
    float kelvinToCelsius(float kelvin);
    float celsiusToKelvin(float celsius);
    double milesToKilometers(double miles);
    double kilometersToMiles(double kilometers);
}
