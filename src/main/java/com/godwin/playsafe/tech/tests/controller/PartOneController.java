package com.godwin.playsafe.tech.tests.controller;


import com.godwin.playsafe.tech.tests.service.api.PartOneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Godwin Tavirimirwa
 * created at 17-Dec-20 15:03
 */
@RestController
@RequestMapping(value = "/conversions/",
        produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class PartOneController {

    private final PartOneService partOneService;

    @Autowired
    public PartOneController(PartOneService partOneService) {
        this.partOneService = partOneService;
    }

    @GetMapping("ktoc")
    public float kelvinToCelsius(@RequestParam float kelvin) {
        log.info("kelvinToCelsius method input {}", kelvin);
        return partOneService.kelvinToCelsius(kelvin);
    }

    @GetMapping("ctok")
    public float celsiusToKelvin(@RequestParam float celsius) {
        log.info("celsiusToKelvin method input: {}", celsius);
        return partOneService.celsiusToKelvin(celsius);
    }

    @GetMapping("mtok")
    public double milesToKm(@RequestParam double miles) {
        log.info("milesToKm method input: {}", miles);
        return partOneService.milesToKilometers(miles);
    }

    @GetMapping("ktom")
    public double kmToMiles(@RequestParam double km) {
        log.info("kmToMiles method input {}", km);
        return partOneService.kilometersToMiles(km);
    }
}
