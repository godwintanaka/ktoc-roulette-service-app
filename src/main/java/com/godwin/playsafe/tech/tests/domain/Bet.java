package com.godwin.playsafe.tech.tests.domain;

/**
 * @author Godwin Tavirimirwa
 * created at 18-Dec-20 09:57
 */
public class Bet {
    private String playerName;
    private String betItem;
    private Double betAmount;
    private boolean win;
    private Double totalAmount;

    public Bet(String playerName, String betItem, Double betAmount, boolean win, Double totalAmount) {
        this.playerName = playerName;
        this.betItem = betItem;
        this.betAmount = betAmount;
        this.win = win;
        this.totalAmount = totalAmount;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getBetItem() {
        return betItem;
    }

    public Double getBetAmount() {
        return betAmount;
    }

    public boolean isWin() {
        return win;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setBetItem(String betItem) {
        this.betItem = betItem;
    }

    public void setBetAmount(Double betAmount) {
        this.betAmount = betAmount;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Bet{");
        sb.append("playerName='").append(playerName).append('\'');
        sb.append(", betItem='").append(betItem).append('\'');
        sb.append(", betAmount=").append(betAmount);
        sb.append(", win=").append(win);
        sb.append(", totalAmount=").append(totalAmount);
        sb.append('}');
        sb.append("\n");
        return sb.toString();
    }
}
