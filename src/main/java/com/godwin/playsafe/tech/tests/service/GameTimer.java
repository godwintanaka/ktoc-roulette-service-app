package com.godwin.playsafe.tech.tests.service;

import org.springframework.scheduling.annotation.EnableAsync;

import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Godwin Tavirimirwa
 * created at 18-Dec-20 16:00
 */
@EnableAsync
public class GameTimer extends TimerTask {

    public static int generatedNumber;

    @Override
    public void run() {
        generatedNumber = ThreadLocalRandom.current().nextInt(0, 37);
    }
}
