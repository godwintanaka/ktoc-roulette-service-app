package com.godwin.playsafe.tech.tests;

import com.godwin.playsafe.tech.tests.service.api.RouletteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleConsoleGameApplication implements CommandLineRunner {

    private final RouletteService rouletteService;

    @Autowired
    public SampleConsoleGameApplication(RouletteService rouletteService) {
        this.rouletteService = rouletteService;
    }

    public static void main(String[] args) {
        SpringApplication.run(SampleConsoleGameApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        rouletteService.playRoulette();
    }
}
