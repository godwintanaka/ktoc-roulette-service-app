package com.godwin.playsafe.tech.tests.service.impl;

import com.godwin.playsafe.tech.tests.constants.GameConstants;
import com.godwin.playsafe.tech.tests.domain.Bet;
import com.godwin.playsafe.tech.tests.domain.PlayerSummary;
import com.godwin.playsafe.tech.tests.service.GameTimer;
import com.godwin.playsafe.tech.tests.service.api.RouletteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static com.godwin.playsafe.tech.tests.service.GameUtils.*;

/**
 * @author Godwin Tavirimirwa
 * created at 17-Dec-20 15:08
 */
@Service
@Slf4j
public class RouletteServiceImpl implements RouletteService {

    @Override
    public void playRoulette() {
        Timer gameTimerInstance = new Timer();
        gameTimerInstance.schedule(new GameTimer(), GameConstants.DELAY, GameConstants.BALL_DURATION);
        List<Bet> playerBetsList = new ArrayList<>();
        Scanner playerInput = new Scanner(System.in);
        List<String> playerNames = new ArrayList<>();
        Set<PlayerSummary> playerSummarySet = new HashSet<>();
        String enteredInput;
        try {
            Resource resource = new ClassPathResource(GameConstants.PLAYER_NAMES_FILE);
            try (BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()))) {
                String line = reader.readLine();
                while (Objects.nonNull(line)) {
                    playerNames.add(line);
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            System.out.println(GameConstants.FILE_ERROR);
        }
        System.out.print(GameConstants.PLAYERS);
        playerNames.forEach(player -> System.out.print(player.concat("\t")));
        System.out.println(GameConstants.GAME_INSTRUCTIONS);
        for (int i = 0; i < playerNames.size(); i++) {
            System.out.print(GameConstants.PROMPT_FOR_INPUT);
            System.out.println(GameConstants.BETTING_PLAYER
                    .concat(playerNames.get(i)).concat(GameConstants.BETTING_PLAYER_SUFFIX) );
            while (playerInput.hasNext()) {
                System.out.print(GameConstants.PROMPT_FOR_INPUT);
                enteredInput = playerInput.nextLine();
                if (enteredInput.equalsIgnoreCase(GameConstants.TERMINATOR)) {
                    System.out.println(GameConstants.BETS_TERMINATED + playerNames.get(i));
                    break;
                } else {
                    try {
                        String[] betContents = enteredInput.split("\\s+");
                        String betItem = betContents[0];
                        if (isValidBetItem(betItem)) {
                            Bet betObject = new Bet(playerNames.get(i), betItem,
                                    Double.valueOf(betContents[1]), false,
                                    GameConstants.DEFAULT_TOTAL_AMOUNT);
                            playerBetsList.add(betObject);
                        } else System.out.println(GameConstants.BET_ITEM_ERROR);
                    } catch (Exception e) {
                        System.out.println(GameConstants.INVALID_INPUT);
                    }
                }
            }
        }
        final int ballNumber = GameTimer.generatedNumber;
        System.out.println(GameConstants.GENERATED_NUMBER + ballNumber);
        analyseBets(playerBetsList, ballNumber);
        playerBetsList.forEach(item -> {
            String finalResult = GameConstants.LOSE;
            if (item.isWin()) finalResult = GameConstants.WIN;
            System.out.printf("%30s%10s%10s%10s%n", item.getPlayerName(), item.getBetItem(),
                    finalResult, item.getTotalAmount().toString());
        });
        createPlayerSummary(playerBetsList, playerSummarySet);
        System.out.printf("%30s%n", GameConstants.OPTIONAL_BONUS);
        System.out.printf("%30s%10s%10s%n", GameConstants.PLAYER, GameConstants.TOTAL_BETS,
                GameConstants.TOTAL_WINS);
        playerSummarySet.forEach(item -> System.out.printf("%30s%10s%10s%n", item.getPlayerName(),
                item.getTotalBet().toString(),
                item.getTotalWin().toString()));
        playerBetsList.clear();
        playerSummarySet.clear();
        System.exit(0);
    }
}
