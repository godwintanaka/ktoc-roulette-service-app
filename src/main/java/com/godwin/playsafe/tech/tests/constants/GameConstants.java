package com.godwin.playsafe.tech.tests.constants;

/**
 * @author Godwin Tavirimirwa
 * created at 19-Dec-20 08:37
 */
public class GameConstants {

    public static final String BET = " Bet ";
    public static final String PLAYER_NAMES_FILE = "players.txt";
    public static final String TUPLE_SIZE = "%30s%10s%10s%10s%n";
    public static final String BET_ITEM_ERROR = "Bet item should be either 1-36, EVEN or ODD";
    public static final String INVALID_INPUT = "Invalid input provided.";
    public static final String GAME_INSTRUCTIONS = "\nTo terminate bets, enter letter x";
    public static final String FILE_ERROR = "Could not read file or file contents";
    public static final String PROMPT_FOR_INPUT = "Enter number (1-36, EVEN or ODD) and bet amount OR 'x' to terminate: ";
    public static final String BETS_TERMINATED = "Terminated bets for ";
    public static final String GENERATED_NUMBER = "Generated number: ";
    public static final String LOSE = "LOSE";
    public static final String WIN = "WIN";
    public static final String OPTIONAL_BONUS = "************************OPTIONAL BONUS QUESTION*************************";
    public static final String PLAYER = "Player ";
    public static final String TOTAL_BETS = " Total Bets ";
    public static final String TOTAL_WINS = " Total Wins ";
    public static final String TERMINATOR = "x";
    public static final String PLAYERS = "Players: ";
    public static final String BETTING_PLAYER = "(Bets for ";
    public static final String BETTING_PLAYER_SUFFIX = "):";
    public static final String OUTCOME = "Outcome";
    public static final String WINNINGS = "Winnings";
    public static final long BALL_DURATION = 30000;
    public static final double DEFAULT_TOTAL_AMOUNT = 0.0;
    public static final long DELAY = 0;


    private GameConstants() {

    }
}
