package com.godwin.playsafe.tech.tests.service.impl;

import com.godwin.playsafe.tech.tests.service.api.PartOneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Godwin Tavirimirwa
 * created at 19-Dec-20 07:14
 */
@Service
@Slf4j
public class PartOneServiceImpl implements PartOneService {
    @Override
    public float kelvinToCelsius(float kelvin) {
        float result = kelvin - 273.15F;
        log.info("kelvinToCelsius result: {}", result);
        return result;
    }

    @Override
    public float celsiusToKelvin(float celsius) {
        float result = celsius + 273.15F;
        log.info("celsiusToKelvin result: {}", result);
        return result;
    }

    @Override
    public double milesToKilometers(double miles) {
        double result = miles * 1.6093;
        log.info("milesToKilometers result: {}", result);
        return result;
    }

    @Override
    public double kilometersToMiles(double kilometers) {
        double result = kilometers * 0.6213;
        log.info("kilometersToMiles result: {}", result);
        return result;
    }
}
