package com.godwin.playsafe.tech.tests.domain;

/**
 * @author Godwin Tavirimirwa
 * created at 18-Dec-20 18:16
 */
public class PlayerSummary {
    public String playerName;
    public Double totalBet;
    public Double totalWin;

    public PlayerSummary() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public Double getTotalBet() {
        return totalBet;
    }

    public Double getTotalWin() {
        return totalWin;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setTotalBet(Double totalBet) {
        this.totalBet = totalBet;
    }

    public void setTotalWin(Double totalWin) {
        this.totalWin = totalWin;
    }

    @Override
    public String toString() {
        return playerName + "," + totalBet + "," + totalWin;
    }
}
