package com.godwin.playsafe.tech.tests.service;

import com.godwin.playsafe.tech.tests.constants.GameConstants;
import com.godwin.playsafe.tech.tests.domain.Bet;
import com.godwin.playsafe.tech.tests.domain.PlayerSummary;

import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author Godwin Tavirimirwa
 * created at 18-Dec-20 23:30
 */
public class GameUtils {
    public static boolean numberValidator(String input) {
        if (Objects.isNull(input) || input.isEmpty()) {
            return false;
        }
        try {
            double doubleInput = Integer.parseInt(input);
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
        return true;
    }

    public static boolean hasPlayerName(final Set<PlayerSummary> playerSummarySet, final String playerName) {
        return playerSummarySet.stream().anyMatch(summary -> summary.getPlayerName().equals(playerName));
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

    public static boolean isValidBetItem(String betItem) {
        if (Objects.isNull(betItem) || betItem.isEmpty()) return false;
        if (betItem.equalsIgnoreCase("even")) {
            return true;
        } else if (betItem.equalsIgnoreCase("odd")) {
            return true;
        } else if (numberValidator(betItem)) {
            int intBetItem = Integer.parseInt(betItem);
            return intBetItem > 0 && intBetItem < 37;
        } else return false;
    }

    public static void createPlayerSummary(List<Bet> playerBetsList, Set<PlayerSummary> playerSummarySet) {
        for (int i = 0; i < playerBetsList.size(); i++) {
            Bet bet = playerBetsList.get(i);
            if (!playerSummarySet.isEmpty()) {
                if (hasPlayerName(playerSummarySet, bet.getPlayerName())) {
                    playerSummarySet.stream().filter(summary -> summary.getPlayerName()
                            .equalsIgnoreCase(bet.getPlayerName())).forEach(
                            summary -> {
                                summary.setTotalBet(summary.getTotalBet() + bet.getBetAmount());
                                if (bet.isWin()) {
                                    summary.setTotalWin(summary.totalWin + bet.getTotalAmount());
                                }
                            }
                    );
                } else {
                    PlayerSummary playerSummary = new PlayerSummary();
                    playerSummary.setPlayerName(bet.getPlayerName());
                    playerSummary.setTotalBet(bet.getBetAmount());
                    playerSummary.setTotalWin(bet.getTotalAmount());
                    playerSummarySet.add(playerSummary);
                }
            } else {
                PlayerSummary playerSummary = new PlayerSummary();
                playerSummary.setPlayerName(bet.getPlayerName());
                playerSummary.setTotalBet(bet.getBetAmount());
                playerSummary.setTotalWin(bet.getTotalAmount());
                playerSummarySet.add(playerSummary);
            }

        }
    }

    public static void analyseBets(List<Bet> playerBetsList, int ballNumber) {
        if (!playerBetsList.isEmpty()) {
            for (Bet bet : playerBetsList) {
                String betItem = bet.getBetItem();
                Double betAmount = bet.getBetAmount();
                if (betItem.equalsIgnoreCase("even")) {
                    if (isEven(ballNumber)) {
                        bet.setWin(true);
                        bet.setTotalAmount(2 * betAmount);
                    }
                } else if (betItem.equalsIgnoreCase("odd")) {
                    if (!isEven(ballNumber)) {
                        bet.setWin(true);
                        bet.setTotalAmount(2 * betAmount);
                    }
                } else if (numberValidator(betItem)) {
                    if (Integer.parseInt(betItem) == ballNumber) {
                        bet.setWin(true);
                        bet.setTotalAmount(36 * betAmount);
                    }
                } else {
                    bet.setWin(false);
                    bet.setTotalAmount(0.0);
                }
            }
        }
        System.out.printf("%30s%n", "Number :" + playerBetsList.size());
        System.out.printf("%30s%10s%10s%10s%n", GameConstants.PLAYER, GameConstants.BET,
                GameConstants.OUTCOME, GameConstants.WINNINGS);
    }

}
